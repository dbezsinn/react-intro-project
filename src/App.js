// @flow
import React, {Component} from 'react';
import { configure } from 'mobx';

import 'antd/dist/antd.css';
import './App.scss';

configure({ enforceActions: "always" });

const isDevelopment = process.env.NODE_ENV === 'development';
const basename = isDevelopment ? '' : '/';

type Props = {};
type State = {};

class App extends Component<Props, State> {
  render() {
    return (
      <h1>This is the very beginning...</h1>
    );
  }
}

export default App;
