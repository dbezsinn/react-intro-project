import db from '../common/mock-data/dbMock';

import type {ApiMockResponse} from "../common/types";

const basePromise = (successData: ApiMockResponse, errorData: ApiMockResponse) =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      errorData ? reject(errorData) : resolve(successData);
    }, 1000);
  });

const loginPromise = (values) => new Promise((resolve, reject) => {
  setTimeout(() => {
    const { userName, userPassword } = values;
    const user = db.users.find(({ userName: name }) => userName === name);

    if (!user) {
      reject({
        data: { userName: 'User with this login is not registered.' }
      });
      return;
    }

    if (userPassword !== user.userPassword) {
      reject({
        data: { userPassword: 'Wrong password.' }
      });
      return;
    }

    resolve({
      data: { token: 'Something like API token here' }
    });
  }, 1000);
});

const userLogin = (values) => loginPromise(values);

const userRegister = (successData: ApiMockResponse, errorData: ApiMockResponse) => basePromise(successData, errorData);

const userLogOut = (successData: ApiMockResponse, errorData: ApiMockResponse) => basePromise(successData, errorData);

const getAllTests = (errorData?: ApiMockResponse) => basePromise(db.tests, errorData);

const exposedMethods = {
  userLogin,
  userRegister,
  userLogOut,
  getAllTests,
};

export default exposedMethods;
