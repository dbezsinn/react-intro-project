const tests = [
  {
    id: 1,
    ownerId: 1,
    title: 'baklajan',
    events: [
      { id: 1, title: 'Destroy Xandar', nextEventId: 2, isStart: true },
      { id: 2, title: 'Gather Power Stone', nextEventId: 3 },
      { id: 3, title: `Kick out Hulk's ass`, nextEventId: 4 },
      { id: 4, title: 'Gather Space Stone', nextEventId: 5 },
      { id: 5, title: 'Simulate own death', nextEventId: 6 },
      { id: 6, title: 'Gather Reality Stone', nextEventId: 7 },
      { id: 7, title: 'Lost someone you love', nextEventId: 8 },
      { id: 8, title: 'Gather Soul Stone', nextEventId: 9 },
      { id: 9, title: 'Gather Time Stone', nextEventId: 10 },
      { id: 10, title: 'Gather Mind Stone', nextEventId: 11 },
      { id: 11, title: `Meet Thor's new axe`, isEnd: true },
    ],
  }
];

const users = [
  { id: 1, userName: 'baklajan', userPassword: '11111111', firstName: 'Thanos', secondName: 'Kartofanos' },
  { id: 2, userName: 'captain-shield', userPassword: '11111111', firstName: 'Steve', secondName: 'Rogers' },
  { id: 3, userName: 'doctor-weirdo', userPassword: '11111111', firstName: 'Steven', secondName: 'Strange' },
  { id: 4, userName: 'red-head', userPassword: '11111111', firstName: 'Wanda', secondName: 'Maximoff' },
];

const db = {
  tests,
  users,
};

export default db;
